function makeToolbox(opts)
arguments
    opts.version string {mustBeTextScalar} = string(missing)
    opts.build_nr string {mustBeTextScalar} = string(missing)
end
    try
        projectFile = "ltbfiles_MATLAB-Toolbox.prj";
        if ismissing(opts.version)
            opts.version = matlab.addons.toolbox.toolboxVersion(projectFile);
        end
        if ~ismissing(opts.build_nr)
            opts.version = strcat(opts.version, ".", opts.build_nr);
        end
        matlab.addons.toolbox.toolboxVersion(projectFile, opts.version);
        matlab.addons.toolbox.packageToolbox(projectFile, strcat("ltbfiles_toolbox_", opts.version));
        exit(0);
    catch except
        disp(except)
        exit(2);
    end
end
