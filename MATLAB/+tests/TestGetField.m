classdef TestGetField < matlab.unittest.TestCase
    
    methods(Test)
        
        function testGetField(self)
            s = struct(a=1, b=struct(c=2));
            self.verifyEqual(tools.get_field(s, "a"), 1);
            self.verifyEqual(tools.get_field(s, "b"), struct(c=2));
            self.verifyEqual(tools.get_field(s, "c"), NaN);
            self.verifyEqual(tools.get_field(s, "b.c"), 2);
            self.verifyEqual(tools.get_field(s, "d", 10), 10);
            self.verifyEqual(tools.get_field(s, "e.c"), NaN);
        end
    end
    
end