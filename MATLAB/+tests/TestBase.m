classdef TestBase < matlab.unittest.TestCase
    
    properties (Constant)
        test_files_folder = fullfile(fileparts(mfilename("fullpath")), "..", "..", "test_files");
    end

end
