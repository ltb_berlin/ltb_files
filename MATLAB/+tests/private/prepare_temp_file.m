function tempfile = prepare_temp_file(ext)
    random_hash = dec2hex(randi(16,1,32)-1)';
    tempfile = fullfile(tempdir, strcat("tmp_", random_hash, ext));
    if isfile(tempfile)
        delete(tempfile);
    end
end