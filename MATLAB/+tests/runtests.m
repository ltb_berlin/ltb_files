function result = runtests(coverage_folder)
if nargin == 0
    coverage_folder = ".";
end
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin
import matlab.unittest.plugins.codecoverage.CoberturaFormat
import matlab.unittest.plugins.codecoverage.CoverageReport
import matlab.unittest.plugins.XMLPlugin

coverageReportFile = "coverage.xml";
regularReport = CoverageReport;
reportFormat = CoberturaFormat(coverageReportFile);
pCover = CodeCoveragePlugin.forFolder(coverage_folder, ...
    Producing=[regularReport, reportFormat], ...
    IncludingSubfolders=true);

resultReportFile = "testResults.xml";
pReport = XMLPlugin.producingJUnitFormat(resultReportFile);

suite = testsuite("tests");
runner = TestRunner.withTextOutput;
runner.addPlugin(pCover);
runner.addPlugin(pReport);
result = runner.run(suite);