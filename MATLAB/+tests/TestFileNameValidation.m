classdef TestFileNameValidation < matlab.unittest.TestCase

    properties (TestParameter)
        validNames = struct( ...
            pureName="file.ext", ...
            relativeName="..\..\file.ext", ...
            absPath="C:\test\name.ext", ...
            pathOnly="C:\test\");
        invalidNames = struct( ...
            badChars="file?.ext");
    end

    methods(Test)
        
        function TestValidNames(self, validNames)
            self.verifyTrue(tools.is_legal_path(validNames));
        end

        function TestInvalidNames(self, invalidNames)
            self.assumeTrue(ispc, "Works on PC only");
            self.verifyFalse(tools.is_legal_path(invalidNames));
        end

    end
    
end
