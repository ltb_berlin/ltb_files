classdef TestLtbFiles < tests.TestBase
    
    properties (Constant)
        test_folder_ltbfiles = tests.TestBase.test_files_folder;

        test_folder_raman = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "raman");
        test_folder_basic = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "spectra_basic");
        test_folder_corrupted = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "corrupted");
        test_folder_advanced = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "spectra_advanced");
        test_folder_exported = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "exported");
        test_folder_raw = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "raw_images");

        libs_file_iso_and_coord = fullfile(tests.TestLtbFiles.test_folder_advanced, "isotime_and_coords.aryx");
        libs_file_steel = fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "spectra_basic", "steel.ary");
    end

    properties (TestParameter)
        raman_file_aryx = struct( ...
            r532=fullfile(tests.TestLtbFiles.test_folder_raman, "Si_R532.aryx"), ...
            r785=fullfile(tests.TestLtbFiles.test_folder_raman, "Si_R785.aryx"));
        raman_file_ary = struct( ...
            r532=fullfile(tests.TestLtbFiles.test_folder_raman, "Si_R532.ary"), ...
            r785=fullfile(tests.TestLtbFiles.test_folder_raman, "Si_R785.ary"));
        aia_file = struct( ...
            mixed=fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "aia_log", "AutoImage.csv"), ...
            iso=fullfile(tests.TestLtbFiles.test_folder_ltbfiles, "aia_log", "AutoImageISO_only.csv"));
    end
    
    methods (Test)
        %% ------------------------ ARY -------------------------------------------        
        function TestLoadAryWithDefaultPars(self)
            [Y, x, o, h] = ltbfiles.read_ltb_ary(self.libs_file_steel);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertMetadataHeaders(h);
            self.verifyEqual(h.filename, self.libs_file_steel);
            self.verifyTrue(issorted(x));
            self.verifyEqual(h.x_name, "Wavelength");
        end
        function TestLoadArySorted(self)
            [Y, x, o, h] = ltbfiles.read_ltb_ary(self.libs_file_steel, sort_wl=true);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertMetadataHeaders(h);
            self.verifyEqual(h.filename, self.libs_file_steel);
            self.verifyTrue(issorted(x));
            self.verifyEqual(h.x_name, "Wavelength");
        end 
        function TestLoadAryUnsorted(self)
            [Y, x, o, h] = ltbfiles.read_ltb_ary(self.libs_file_steel, sort_wl=false);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertMetadataHeaders(h);
            self.verifyEqual(h.filename, self.libs_file_steel);
            self.verifyFalse(issorted(x));
            self.verifyEqual(h.x_name, "Wavelength");
        end
        function TestLoadAryCorruptedRep(self)
            filename = fullfile(self.test_folder_corrupted, "al_empty_rep.ary");
            [Y, x, o, h] = ltbfiles.read_ltb_ary(filename);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertMetadataHeaders(h);
            self.verifyEqual(h.filename, string(filename));
            self.verifyTrue(isnat(h.timestamp));
            self.verifyEqual(h.x_name, "Wavelength");
        end
        function TestLoadAryWithCoordinates(self)
            filename = fullfile(self.test_folder_advanced, "brass_XPos_123_YPos_321.ary");
            [Y, x, o, h] = ltbfiles.read_ltb_ary(filename);
            self.assertMetadataHeaders(h);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.verifyEqual(h.filename, string(filename));
            self.verifyEqual(h.posX, 123);
            self.verifyEqual(h.posY, 321);
            self.verifyEqual(h.x_name, "Wavelength");
        end
        function TestLoadAryRaman(self, raman_file_ary)
            [Y, x, ~, h] = ltbfiles.read_ltb_ary(raman_file_ary);
            self.verifyEqual(h.x_name, "Raman shift");
            iStokes = find((x > 80) & (x < 1400));
            [~,iMax] = max(Y(iStokes));
            xMax = x(iStokes(iMax));
            self.verifyEqual(xMax, 521, AbsTol=1);
        end
        
%% ------------------------ ARYX ------------------------------------------       
        function TestLoadAryxDefault(self)
            [Y, x, o, h] = ltbfiles.read_ltb_aryx(self.libs_file_iso_and_coord);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertMetadataHeaders(h);
            self.verifyEqual(h.filename, self.libs_file_iso_and_coord);
            self.verifyTrue(issorted(x));
            self.verifyEqual(h.posX, 2582);
            self.verifyEqual(h.posY, 566);
            self.verifyTrue(isa(h.timestamp, "datetime"));
            self.verifyEqual(h.x_name, "Wavelength");
        end
        function TestLoadAryxSorted(self)
            [Y, x, o, h] = ltbfiles.read_ltb_aryx(self.libs_file_iso_and_coord, sort_wl=true);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertTrue(ismember("filename", h.Properties.VariableNames));
            self.verifyEqual(h.filename, self.libs_file_iso_and_coord);
            self.verifyTrue(issorted(x));
            self.assertTrue(ismember("posX", h.Properties.VariableNames));
            self.assertTrue(ismember("posY", h.Properties.VariableNames));
            self.verifyEqual(h.x_name, "Wavelength");
        end 
        function TestLoadAryxUnsorted(self)
            [Y, x, o, h] = ltbfiles.read_ltb_aryx(self.libs_file_iso_and_coord, sort_wl=false);
            self.verifyEqual(numel(x),numel(o));
            self.verifyEqual(numel(x),numel(Y));
            self.assertTrue(istable(h));
            self.assertTrue(ismember("filename", h.Properties.VariableNames));
            self.verifyEqual(h.filename, self.libs_file_iso_and_coord);
            self.verifyFalse(issorted(x));
            self.assertTrue(ismember("posX", h.Properties.VariableNames));
            self.assertTrue(ismember("posY", h.Properties.VariableNames));
            self.verifyEqual(h.x_name, "Wavelength");
        end
        function TestLoadAryxRaman(self, raman_file_aryx)
            [Y, x, ~, h] = ltbfiles.read_ltb_aryx(raman_file_aryx);
            self.assertMetadataHeaders(h);
            self.verifyEqual(h.x_name, "Raman shift");
            iStokes = find((x > 80) & (x < 1400));
            [~,iMax] = max(Y(iStokes));
            xMax = x(iStokes(iMax));
            self.verifyEqual(xMax, 521, AbsTol=1);
        end
        function TestLoadAryxSummaryV1(self)
            filename = fullfile(self.test_folder_advanced, "with_summary_v1.aryx");
            [~,~,~,h] = ltbfiles.read_ltb_aryx(filename);
            self.assertMetadataHeaders(h);
            kernelTime = datetime(h.timeStampKernelStart, InputFormat="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", TimeZone="UTC");
            self.verifyEqual(year(kernelTime), 2024);
            aiaTime = datetime(h.aiaTimeStamp, InputFormat="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", TimeZone="UTC");
            self.verifyEqual(year(aiaTime), 2023);
        end
        function TestLoadAryxMissingMetadata(self)
            filename = fullfile(self.test_folder_corrupted, "metadata_incomplete.aryx");
            self.verifyWarning(@()ltbfiles.read_ltb_aryx(filename), "ltbfiles:read_ltb_aryx:metadata_missing");
            filename = fullfile(self.test_folder_corrupted, "metadata_missing.aryx");
            self.verifyError(@()ltbfiles.read_ltb_aryx(filename), "ltbfiles:read_ltb_aryx:metadata_missing");
            filename = fullfile(self.test_folder_corrupted, "metadata_empty.aryx");
            self.verifyError(@()ltbfiles.read_ltb_aryx(filename), "ltbfiles:read_ltb_aryx:metadata_missing");
            filename = fullfile(self.test_folder_advanced, "with_summary_v1_timestamps_missing.aryx");
            [~,~,~,h] = self.verifyWarningFree(@()ltbfiles.read_ltb_aryx(filename));
            self.verifyEqual(h.timeStampKernelStart, "");
            self.verifyEqual(h.aiaTimeStamp, "");
        end
        function TestLoadAryxBadSummaryVersion(self)
            filename = fullfile(self.test_folder_corrupted, "bad_summary_version.aryx");
            self.verifyError(@()ltbfiles.read_ltb_aryx(filename), "ltbfiles:read_ltb_aryx:load_metadata_versioned:unknown_version");
        end
        function TestWriteAryx(self)
            [Y, x, o, h] = ltbfiles.read_ltb_aryx(self.libs_file_iso_and_coord);
            tempfile = prepare_temp_file(".aryx");
            ltbfiles.write_ltb_aryx(tempfile, Y, x, o, h);
            cleanup = onCleanup(@() delete(tempfile));
            [Yl, xl, ol, hl] = ltbfiles.read_ltb_aryx(tempfile);
            self.verifyEqual(Yl, Y);
            self.verifyEqual(xl, x);
            self.verifyEqual(ol, o);
            self.verifyEqual(hl(:,["timestamp", "posX", "posY"]), h(:,["timestamp", "posX", "posY"]))
        end
        function TestWriteAryxSummary(self)
            [Y, x, o, h] = ltbfiles.read_ltb_aryx(fullfile(self.test_folder_advanced, "with_summary_v1.aryx"));
            tempfile = prepare_temp_file(".aryx");
            ltbfiles.write_ltb_aryx(tempfile, Y, x, o, h);
            cleanup = onCleanup(@() delete(tempfile));
            [Yl, xl, ol, hl] = ltbfiles.read_ltb_aryx(tempfile);
            self.verifyEqual(Yl, Y);
            self.verifyEqual(xl, x);
            self.verifyEqual(ol, o);
            self.verifyEqual(hl(:,["timestamp", "posX", "posY"]), h(:,["timestamp", "posX", "posY"]))
        end
        function TestWriteAryxRaman(self, raman_file_aryx)
            [Y, x, o, h] = ltbfiles.read_ltb_aryx(raman_file_aryx);
            tempfile = prepare_temp_file(".aryx");
            ltbfiles.write_ltb_aryx(tempfile, Y, x, o, h);
            cleanup = onCleanup(@() delete(tempfile));
            [Yl, xl, ol, hl] = ltbfiles.read_ltb_aryx(tempfile);
            self.verifyEqual(Yl, Y);
            self.verifyEqual(xl, x);
            self.verifyEqual(ol, o);
            self.verifyEqual(hl(:,["timestamp", "posX", "posY"]), h(:,["timestamp", "posX", "posY"]))
        end

%% ----------------------- TXT --------------------------------------------
        function TestLoadExportedTxt(self)
            filename = fullfile(self.test_folder_exported, "LIBS_3_1_004_XPos_3316_YPos_242.txt");
            [Y, x, h] = ltbfiles.read_exported_txt(filename);
            self.assertMetadataHeaders(h);
            self.verifyTrue(all(0 <= diff(x)));
            self.verifySize(Y, size(x));
            self.verifyEqual(h.posX, 3316);
            self.verifyEqual(h.posY, 242);
            self.verifyEqual(h.filename, filename);
            self.verifyTrue(isnat(h.timestamp));
        end

%% ----------------------- ELI --------------------------------------------
        function TestLoadEli(self)
            filename = fullfile(self.test_folder_advanced, "Ne_calibration.eli");
            [spec, head] = ltbfiles.read_ltb_eli(filename);
            self.verifySize(spec, [head.width, 1]);
            self.verifyFalse(any(isnan(spec)));
        end

%% ----------------------- ESS --------------------------------------------
        function TestLoadEss(self)
            filename = fullfile(self.test_folder_advanced, "Li_610.ess");
            [y, x, head] = ltbfiles.read_ltb_ess(filename);
            self.verifySize(y, [head.width, 1]);
            self.verifyEqual(size(y), size(x));
            self.verifyFalse(any(isnan(y)));
        end

%% ---------------------- RAWX --------------------------------------------
        function TestLoadRawx(self)
            filename = fullfile(self.test_folder_raw, "deuterium.rawx");
            [img, head] = ltbfiles.read_ltb_rawx(filename);
            self.verifyEqual(ndims(img), 2);
            self.verifyTrue(isstruct(head));
        end

%% ----------------------- RAW --------------------------------------------
        function TestLoadRaw(self)
            filename = fullfile(self.test_folder_raw, "deuterium.raw");
            [img, head] = ltbfiles.read_ltb_raw(filename);
            self.verifyEqual(ndims(img), 2);
            self.verifyTrue(isstruct(head));
        end

%% ----------------------- RAWB -------------------------------------------
        function TestLoadRawB(self)
            filename = fullfile(self.test_folder_raw, "deuterium.rawb");
            [img, head] = ltbfiles.read_ltb_rawb(filename);
            self.verifySize(img, [head.ImgHeight, head.ImgWidth]);
        end

%% ----------------- SCAN FOR FILES --------------------------------------
        function Test_scan_for_files_default(self)
            files = ltbfiles.scan_for_files(fullfile(self.test_folder_ltbfiles, "spectra_basic"));
            self.verifyEqual(numel(files), 3);
        end

        function Test_scan_for_files_subset(self)
            files = ltbfiles.scan_for_files(fullfile(self.test_folder_ltbfiles, "spectra_basic"), extensions=".ary");
            self.verifyEqual(numel(files), 1);
        end

        function Test_scan_for_files_recursive(self)
            files = ltbfiles.scan_for_files(self.test_folder_ltbfiles, recursive=true);
            self.verifyGreaterThan(numel(files), 3);
        end

%% ------------------- MASS LOADERS ---------------------------------------
        function Test_load_files(self)
            files = fullfile(self.test_folder_ltbfiles, "spectra_basic", ["noise.aryx", "whitelight.aryx"]);
            [Y, x, o, h] = ltbfiles.load_files(files);
            self.verifyTrue(isequal(h.filename, files'));
            self.verifySize(Y, [numel(x), numel(files)]);
            self.verifySize(o, size(x));
        end

        function Test_load_files_interpolate(self)
            files = ltbfiles.scan_for_files(fullfile(self.test_folder_ltbfiles, "spectra_basic"));
            self.verifyError(@()ltbfiles.load_files(files), "ltbfiles:load_files:interpolation_required");
            [Y,x] = ltbfiles.load_files(files, interpolate=true);
            self.verifySize(Y, [numel(x), numel(files)]);
        end

        function Test_load_files_error(self)
            file = fullfile(self.test_folder_ltbfiles, "raw_images", "deuterium.raw");
            self.verifyError(@()ltbfiles.load_files(file), "ltbfiles:load_files:unknown_extension");
        end

        function Test_load_folder(self)
            [Y,x] = ltbfiles.load_folder(fullfile(self.test_folder_ltbfiles, "spectra_basic"), extensions=".aryx");
            self.verifySize(Y, [numel(x),2]);
        end

        function Test_load_folder_interpolate(self)
            [Y,x] = ltbfiles.load_folder(fullfile(self.test_folder_ltbfiles, "spectra_basic"), interpolate=true);
            self.verifySize(Y, [numel(x),3]);
        end

%% --------------------- AIA LOG -------------------------------------------
        function Test_load_aia_log(self, aia_file)
            aia = ltbfiles.read_aia_log(aia_file);
            self.assertEqual(string(aia.TimeStamp.TimeZone), "UTC");
            self.verifyEqual(hour(aia.TimeStamp(end)), 14);
            self.verifyFalse(any(isnat(aia.TimeStamp)));
        end

    end

    methods (Hidden)

        function assertMetadataHeaders(self, metadata)
            n_stored = numel(ltbfiles.FileConstants.METADATA_HEADERS_V1);
            % Test for stored metadata
            self.assertTrue(all(...
                strcmp(ltbfiles.FileConstants.METADATA_HEADERS_V1, ...
                    metadata.Properties.VariableNames(1:n_stored) ...
                ) ...
            ));
            % Test for added metadata
            self.assertTrue(all(strcmp(ltbfiles.FileConstants.METADATA_ADDED, ...
                metadata.Properties.VariableNames(n_stored+1:end))));
        end

    end
end
