function bool = is_legal_path(str)
%IS_LEGAL_PATH Check if a string is a valid representation of a path on the current platform.
% SYNOPSIS
%   bool = is_legal_path(str)
% INPUTS
%   str: A string that is supposed to represent a path. The path is not required to actually exist.
% OUTPUTS
%   bool: true is `str` is a valid path or `false` otherwise
%
% https://de.mathworks.com/matlabcentral/answers/44165-check-if-filename-is-valid#answer_417138
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
bool = true;
try
    java.io.File(str).toPath;
catch
    bool = false;
end
