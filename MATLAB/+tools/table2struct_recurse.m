function out_struct = table2struct_recurse(in_table, sub_group, opts)
%TABLE2STRUCT_RECURSE Convert a table with separator structured variable names into a nested struct
% SYNOPSIS
%   out_struct = table2struct_recurse(in_table, sub_group, name,value)
% INPUTS
%   in_table: The table to be converted
%   sub_group: Optional (missing) - Only convert columns that start with the given sub_group name.
% NAME-VALUE-ARGUMENTS
%   separator: Optional ("_") - The separator used to separate groups in the column names.
%
% Be aware that groups should either contain sub groups or a scalar value. A table with variable names "a", "a_b" would
% result in a struct with a value for key "a" being a sub-struct with a key "b". The value for "a" would be lost.
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
arguments
    in_table {mustBeA(in_table, "table")}
    sub_group (1,1) string {mustBeTextScalar} = missing
    opts.separator (1,1) string {mustBeTextScalar} = "_"
end
    all_headers = string(in_table.Properties.VariableNames);
    if ismissing(sub_group)
        ind_plain = ~contains(all_headers, opts.separator);
        out_struct = table2struct(in_table(:, ind_plain));
        headers_recurse = all_headers(~ind_plain);
        sub_groups = unique(extractBefore(headers_recurse, opts.separator));
        for sub = sub_groups
            if isfield(out_struct, sub)
                warning("tools:table2struct_recurse:overwrite_key", ...
                    "Key '%s' is already assigned and will be overwritten by a sub group", sub);
            end
            out_struct.(sub) = recursive_split(in_table(:, ~ind_plain), sub, separator=opts.separator);
        end
    else
        out_struct = recursive_split(in_table, sub_group, separator=opts.separator);
    end
end

function out_struct = recursive_split(in_table, current, opts)
arguments
    in_table {mustBeA(in_table, "table")}
    current (1,1) string {mustBeTextScalar}
    opts.separator (1,1) string {mustBeTextScalar}
end
    all_headers = string(in_table.Properties.VariableNames);
    ind_current = startsWith(all_headers, current);
    if ~any(ind_current)
        out_struct = struct();
        return
    end
    current_names = all_headers(ind_current);
    current_sub = extractAfter(current_names, strcat(current, opts.separator));

    curr_table = in_table(:, ind_current);
    curr_table.Properties.VariableNames = current_sub;
    ind_has_sub = contains(current_sub, opts.separator);
    out_struct = table2struct(curr_table(:, ~ind_has_sub));
    
    sub_names = current_sub(ind_has_sub);
    sub_groups = extractBefore(sub_names, opts.separator);
    sub_groups_unique = unique(sub_groups)';
    for sub = sub_groups_unique
        if isfield(out_struct, sub)
            warning("tools:table2struct_recurse:overwrite_key", ...
                "Key '%s' is already assigned and will be overwritten by a sub group", sub);
        end
        out_struct.(sub) = recursive_split(curr_table(:,ind_has_sub), sub, ...
            separator=opts.separator);
    end
end
