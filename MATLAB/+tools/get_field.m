function val = get_field(s, name, default)
%GET_FIELD Get the value of a struct field, if it exists, otherwise return a default value
% SYNOPSIS
%   val = get_field(s, name, default)
% INPUTS
%   s: The struct to be searched
%   name: the name of the field to be extracted. May be "a" or "a.b" in order to extract from deeper nested sub
%       structures.
%   default: Optional (NaN) - The default value to be returned, if the field is missing.
% OUTPUT
%   val: The value searched
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
arguments
    s {mustBeA(s, "struct")}
    name (1,1) string {mustBeTextScalar}
    default = NaN
end
val = recursive_search(s, name, default);
end

function val = recursive_search(s, name, default)
    parts = split(name, ".");
    if isfield(s, parts(1))
        if isscalar(parts)
            val = s.(parts);
        else
            val = recursive_search(s.(parts(1)), join(parts(2:end), "."), default);
        end
    else
        val = default;
    end
end
