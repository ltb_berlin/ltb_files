function in_table = convert_char_columns_to_strings(in_table)
%CONVERT_CHAR_COLUMNS_TO_STRINGS Convert a table to use string type for any text column
% SYNOPSIS
%   in_table = convert_char_columns_to_strings(in_table)
% INPUTS
%   in_table: The table to be converted
% OUTPUTS
%   in_table the converted table
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
arguments
    in_table {mustBeA(in_table, "table")}
end
    col_names = in_table.Properties.VariableNames;
    indCellstr = varfun(@iscellstr, in_table, output="uniform");
    for iCol = 1:numel(col_names)
        if indCellstr(iCol)
            in_table.(col_names{iCol}) = convertCharsToStrings(in_table.(col_names{iCol}));
        end
    end
end
