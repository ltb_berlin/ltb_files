function [y, x, o, metadata]=read_ltb_ary(filename, opts)
%read_ltb_ary	This function reads data from binary *.ary files for LTB spectrometers.
% 
% Syntax: [y, x, o, metadata]=read_ltb_ary(filename, name-value)
%
% Inputs:
%    filename - Name of the *.ary file to be read. May be a relative path
%               or full filename.
% name, value arguments:
%    sort_wl  - OPTIONAL flag, if spectra should be sorted by their
%               wavelength after reading. default: true
%
% Outputs:
%    y    - Intensity
%    x    - Wavelengths
%    o    - Spectral order of the current pixel
%    metadata - additional file header (table)
%
%   Caution! Due to order overlap, it may happen that two pixels have the
%   same wavelength. If this causes problems in later data treatment, such
%   pixels should be removed using 
%   [x,ind]=unique(x);
%   o=o(ind);
%   y=y(ind);
% 
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    arguments
        filename string {mustBeTextScalar, mustBeFile}
        opts.sort_wl (1,1) logical = true
    end
    % Avoid collision with other instances accessing the same folder by using
    % UUID for unzip folders
    % (old) http://www.briandalessandro.com/blog/how-to-create-a-random-uuid-in-matlab/
    % for compatibility with Octave:
    % http://codegolf.stackexchange.com/questions/58442/generate-random-uuid/58761
    extractFolder = strcat("unzipped-", dec2hex(randi(16,1,32)-1)');
    full_folder = fullfile(tempdir, extractFolder);
    %remove possible old unzipped folder
    if exist(full_folder, "dir"), rmdir(full_folder, "s"); end

    %extract (will raise an error if target dir is write protected)
    files = string(unzip(filename, full_folder));
    cleanup = onCleanup(@() rmdir(full_folder, "s"));
    [~, ~, exts] = fileparts(files);

    %identify spectra file
    ind = strcmpi(".~tmp", exts);
    values = load_values(files(ind));

    %identify order file
    ind=strcmpi(".~aif", exts);
    aif = load_ranges_info(files(ind));
    
    %load metadata
    ind = strcmpi(".~rep", exts);
    metadata = load_metadata(files(ind), filename);

    spec = reshape(values, 2, [])';
    if metadata.ramanExcitationWavelength > 0
        x = (1e7 / metadata.ramanExcitationWavelength) - (1e7 ./ spec(:,2));
        metadata.x_name = "Raman shift";
        metadata.x_unit = "cm^{-1}";
    else
        x = spec(:, 2);
        metadata.x_name = "Wavelength";
        metadata.x_unit = "nm";
    end
    y = spec(:, 1);
    ranges = [aif{1} aif{2}];
    o = NaN(size(x));
    assert(isequal(numel(aif{1}), numel(aif{2}), numel(aif{3})), "Order numbers and ranges have different lengths");
    for i=1:numel(aif{3})
        o((ranges(i,1):ranges(i,2))'+1) = aif{3}(i);
    end

    if opts.sort_wl
        [x, ind] = unique(x);
        o = o(ind);
        y = y(ind);
    end
end

function values = load_values(file)
    fid = fopen(file, "r");
    cleanup = onCleanup(@() fclose(fid));
    values = fread(fid, inf, "float", 0, "l")';
end

function R = load_ranges_info(file)
    %open and read additional information
    fid = fopen(file, "rb");
    cleanup = onCleanup(@() fclose(fid));
    %# type and size in byte of the record fields
    recordType = ["uint" "uint" "uint16" "uint16" "uint16" "uint16" "float" "float"];
    recordLen = [4 4 2 2 2 2 4 4];
    skip_lens = sum(recordLen) - recordLen;
    R = cell(1, numel(recordType));

    %# read column-by-column
    for i=1:numel(recordType)
        %# seek to the first field of the first record
        fseek(fid, sum(recordLen(1:i-1)), "bof");

        %# % read column with specified format, skipping required number of bytes
        R{i} = fread(fid, Inf, recordType{i}, skip_lens(i), "l");
    end
end

function metadata = load_metadata(head_file, filename)
    head = struct;
    fid = fopen(head_file);
    cleanup = onCleanup(@() fclose(fid));
    while ~feof(fid)
        str = fgetl(fid);
        if isempty(str) || isnumeric(str) || strcmp("[end of file]", str)
            break
        end
        vals = textscan(str, "%s %s", "delimiter", "=");
        if isempty(vals{2})
            vals{2} = "";
        end
        name = strrep(string(vals{1}), " ", "_");
        [name, value] = convert_entry(name, string(vals{2}));
        head.(name) = value;           
    end
    try
        timestamp = datetime(strcat(head.date_of_measurement, " " , head.time_of_measurement), InputFormat="dd.MM.yyyy HH:mm:ss.SSS", TimeZone="local");
        timestamp.TimeZone = "UTC";
        time_str = string(timestamp, ltbfiles.FileConstants.TIMESTAMP_FORMAT);
    catch
        timestamp = NaT;
        time_str = ltbfiles.FileConstants.TIMESTAMP_MISSING;
    end
    [posX, posY] = ltbfiles.extract_scan_position(filename);
    gf = @tools.get_field;
    if contains(gf(head, "Scaling", ""), "wavelength")
        raman_excitation = NaN;
    else
        raman_excitation = gf(head, "Raman_excitation_wavelength");
    end
    metadata = table(1, "", gf(head, "spectrometer_serial_number", ""), gf(head, "spectrometer_name", ""), gf(head, "software_version", ""), ...
        ltbfiles.FileConstants.TIMESTAMP_MISSING, time_str, raman_excitation, "AtRest", posX, ...
        posY, NaN, NaN, NaN, NaN, ...
        "", "", "", NaN, gf(head, "exposure_time"), ...
        "Open", false, NaN, gf(head, "AutoImage_HorizontalOffset"), gf(head, "AutoImage_VerticalOffset"), ...
        gf(head, "AutoImage_NumberofComparedLines", 0), gf(head, "AutoImage_TimeStamp", ltbfiles.FileConstants.TIMESTAMP_MISSING), gf(head, "horizontal_binning"), gf(head, "vertical_binning"), gf(head, "HSSpeed"), ...
        gf(head, "ICCD_gain"), gf(head, "EMCCDGain"), gf(head, "delay_time"), gf(head, "Laser_qswitch_delay"), NaN, ...
        NaN, gf(head, "ICCD_gate_width"), NaN, NaN, NaN, ...
        gf(head, "Laser_frequency"), NaN, NaN, gf(head, "Number_of_averaged_spectra"), true, ...
        NaN, NaN, NaN, NaN, NaN, ...
        VariableNames=ltbfiles.FileConstants.METADATA_HEADERS_V1);
    metadata.filename = filename;
    metadata.timestamp = timestamp;
end

function [name, value] = convert_entry(name, value)
    switch name
        case {"Number_of_averaged_spectra", "exposure_time", "pixel_clock_time", ...
              "VSSpeed", "HSSpeed", "ADCBitDepth", "delay_time", ...
              "ICCD_gate_width", "ICCD_gain", "ICCD_gate_mode", "vertical_binning", ...
              "horizontal_binning", "pixel_size", "AutoImage_VerticalOffset", ...
              "AutoImage_HorizontalOffset", "AutoImage_NumberofComparedLines", "EMCCDGain", ...
              "Laser_qswitch_delay", "Laser_frequency"}
            value = str2double(value);
        case "Raman_exitation_wavelength"
            name= "Raman_excitation_wavelength";
            value = str2double(value);
        case "titel_of_measurement"
            name = "title_of_measurement";
        case {"is_EMCCD", "is_ICCD", "EMModeActivated"}
            value = strcmpi(value, "true");
    end
end
