function [Y, x, o, metadata] = load_folder(foldername, opts)
%LOAD_FOLDER This function loads all spectra within a folder into a combined matrix.
%
% Syntax: [Y, x, o, metadata] = LOAD_FOLDER(filenames, name_value_args)
%
% Inputs:
%   filenames - List of files to be loaded. May be relative paths, files in
%               the current workspace or fully qualified filenames
%               including paths. In the latter case, files can be loaded
%               from different locations.
% name, value arguments:
%    extensions - list of file extensions to search for. Defaults to
%                [".ary", ".aryx"]
%    recursive  - specify if the search should be done recursive, searching
%                all sub folders as well. Default: false
%    interpolate - Specify if the function should interpolate spectra if
%                  necessary. Will fail otherwise.
%
% Outputs:
%   Y - Matrix of intensity values. Spectra are ordered column wise.
%   x - Unified wavelength axis for all spectra
%   o - Unified order list
%   metadata - measurement settings and other information stored in the
%   files
%
% If the wavelength axis x differs across the spectra, the name-value argument
% 'interpolate' is used to decide if interpolation should be done or if the
% function should raise an exception.
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

arguments
    foldername string {mustBeTextScalar, mustBeFolder}
    opts.extensions string {mustBeText} = [".ary", ".aryx"]
    opts.interpolate (1,1) logical = false
    opts.recursive (1,1) logical = false
end

files = ltbfiles.scan_for_files(foldername, extensions=opts.extensions, recursive=opts.recursive);
[Y, x, o, metadata] = ltbfiles.load_files(files, interpolate=opts.interpolate);
