function [head, head_end] = read_head_eli_ess(content)
%READ_HEAD_ELI_ESS Combined header interpreter for eli and ess files. Internal, not for public use.
arguments
    content (:,1) string {mustBeText}
end
    number_headers = ["averaging_counts", "exposure_time", "delay_time", "pixel_clock_time", "width", "height", ...
        "center_wave", "Pixeldispersion", "Pixeldispersion_fm", "First_calibration_point_lambda", "First_calibration_point_pixel", ...
        "Second_calibration_point_lambda", "Second_calibration_point_pixel", "Third_calibration_point_lambda", ...+
        "Third_calibration_point_pixel", "Square_scaling"];
    function append_head_value(name, value)
        name = strrep(name, " ", "_");
        name = strrep(name, "-", "_");
        name = strrep(name, "_/_", "_");
        if ismember(name, number_headers)
            head.(name) = str2double(value);
        else
            head.(name) = value;
        end
    end
    
    head = struct;
    iLine = 1;
    while iLine <= size(content, 1)
        tag = regexp(content(iLine), "\[(.+?)\]", "tokens", "once");
        if ~isempty(tag)
            dispersion = regexp(tag, "(Pixeldispersion)= *(\d*\.\d*)", "tokens", "once");
            if ~isempty(dispersion)
                append_head_value(dispersion(1), dispersion(2));
                head_end = iLine;
                break
            elseif strcmp(tag, "Spectrum: wavelength; intensity")
                head_end = iLine;
                break
            end
            iLine = iLine + 1;
            append_head_value(tag, content(iLine));
            iLine = iLine +1;
            continue
        end
        shape = regexp(content(iLine), "(\d+) x (\d+)", "tokens", "once");
        if ~isempty(shape)
            append_head_value("width", shape(1));
            append_head_value("height", shape(2));
        end
        iLine = iLine + 1;
    end
end