function vals = empty_to_nan(vals)
%EMPTY_TO_NAN Convert empty cells to nan values. This is intended to be used with `varfun` on tables.
% SYNOPSIS
%   vals = empty_to_nan(vals)
% INPUTS
%   vals: Values of a table column
% OUTPUTS
%   vals: The same values as the input, where empty sub-cells are replaced by NaN.
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

    function ind = is_empty_cell(val)
        ind = iscell(val) && isempty(val{1});
    end
    if iscell(vals)
        indNull = arrayfun(@is_empty_cell, vals);
        for i = find(indNull)'
            vals{i} = NaN;
        end
        vals = cell2mat(vals);
    end
end
