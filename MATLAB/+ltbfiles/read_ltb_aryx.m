function [y, x, o, metadata]=read_ltb_aryx(filename, opts)
%read_ltb_aryx	This function reads data from binary *.aryx files for LTB spectrometers.
% 
% Syntax: [y, x, o, metadata]=read_ltb_aryx(filename, name-value)
%
% Inputs:
%    filename - Name of the *.aryx file to be read. May be a relative path
%               or full filename.
% name, value arguments:
%    sort_wl  - OPTIONAL flag, if spectra should be sorted by their
%               wavelength after reading. default: true
%
% Outputs:
%    y    - Intensity
%    x    - Wavelengths
%    o    - Spectral order of the current pixel
%    metadata - additional file header (table)
%
%   Caution! Due to order overlap, it may happen that two pixels have the
%   same wavelength. If this causes problems in later data treatment, such
%   pixels should be removed using 
%   [x,ind]=unique(x);
%   o=o(ind);
%   y=y(ind);
% 
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    arguments
        filename string {mustBeTextScalar, mustBeFile}
        opts.sort_wl (1,1) logical = true
    end
    % Avoid collision with other instances accessing the same folder by using
    % UUID for unzip folders
    % (old) http://www.briandalessandro.com/blog/how-to-create-a-random-uuid-in-matlab/
    % for compatibility with Octave:
    % http://codegolf.stackexchange.com/questions/58442/generate-random-uuid/58761
    extractFolder = strcat("unzipped-", dec2hex(randi(16,1,32)-1)');
    full_folder = fullfile(tempdir, extractFolder);
    %remove possible old unzipped folder
    if exist(full_folder, "dir"), rmdir(full_folder, "s"); end

    %extract (will raise an error if target dir is write protected)
    files = string(unzip(filename, full_folder));
    cleanup = onCleanup(@() rmdir(full_folder, "s"));
    [~, names, exts] = fileparts(files);

    %identify spectra file
    ind = strcmpi(".~tmp", exts);
    values = load_values(files(ind));

    %identify order file
    ind=strcmpi(".~aif", exts);
    aif = load_ranges_info(files(ind));

    %read metadata
    ind = strcmpi("metadata_summary", names) & strcmpi(".json", exts);
    if any(ind)
        metadata = load_metadata_versioned(files(ind));
    else
        ind = strcmpi(".~json", exts);
        assert(nnz(ind) == 1, "ltbfiles:read_ltb_aryx:metadata_missing", "No metadata in file '%s'", filename);
        metadata = load_metadata_legacy(files(ind));
    end
    metadata.filename = filename;
    metadata.timestamp = datetime(metadata.timeStampMeasurement, ...
        InputFormat="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", TimeZone="UTC");

    spec = reshape(values, 2, [])';
    if metadata.ramanExcitationWavelength > 0
        metadata.x_name = "Raman shift";
        metadata.x_unit = "cm^{-1}";
        x = (1e7 / metadata.ramanExcitationWavelength) - (1e7 ./ spec(:,2));
    else
        metadata.x_name = "Wavelength";
        metadata.x_unit = "nm";
        x = spec(:, 2);
    end

    y = spec(:, 1);
    ranges = [aif{1} aif{2}];
    o = NaN(size(x));
    if all(diff(ranges, 1, 2) > 0)
        assert(isequal(numel(aif{1}), numel(aif{2}), numel(aif{3})), "Order numbers and ranges have different lengths");
        for i=1:numel(aif{3})
            o((ranges(i,1):ranges(i,2))'+1) = aif{3}(i);
        end
    end

    if opts.sort_wl
        [x, ind] = unique(x);
        o = o(ind);
        y = y(ind);
    end
end

function values = load_values(file)
    fid = fopen(file, "r");
    cleanup = onCleanup(@() fclose(fid));
    values = fread(fid, inf, "double", 0, "l")';
end

function aif = load_ranges_info(file)
    fid=fopen(file,"rb");
    cleanup = onCleanup(@() fclose(fid));
    %# type and size in byte of the record fields
    skip_lens = sum(ltbfiles.FileConstants.ARYX_AIF_BYTES) - ltbfiles.FileConstants.ARYX_AIF_BYTES;
    aif = cell(1, numel(skip_lens));

    %# read column-by-column
    for i=1:numel(skip_lens)
        %# seek to the first field of the first record
        fseek(fid, sum(ltbfiles.FileConstants.ARYX_AIF_BYTES(1:i-1)), "bof");

        %# % read column with specified format, skipping required number of bytes
        aif{i} = fread(fid, Inf, ltbfiles.FileConstants.ARYX_AIF_TYPES(i), skip_lens(i), "l");
    end
end

function metadata = load_metadata_versioned(meta_file)
    head_json = jsondecode(fileread(meta_file, encoding="UTF-8"));
    switch head_json.version
        case 1
            metadata = tools.convert_char_columns_to_strings(struct2table(head_json, AsArray=true));
            names = metadata.Properties.VariableNames;
            metadata = varfun(@ltbfiles.empty_to_nan, metadata, OutputFormat="table");
            metadata.Properties.VariableNames = names;
        otherwise
            error("ltbfiles:read_ltb_aryx:load_metadata_versioned:unknown_version", ...
                "Metadata version %d is unknown", head_json.version);
    end
end

function metadata = load_metadata_legacy(meta_file)
    head = jsondecode(fileread(meta_file, encoding="UTF-8"));
    metadata = ltbfiles.convert_legacy_metadata_struct(head);
end
