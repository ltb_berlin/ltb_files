function file_list = scan_for_files(base_folder, opts)
%SCAN_FOR_FILES Searches a folder and optionally all sub folders for files
%with a given extension.
%
% Syntax: file_list = SCAN_FOR_FILES(base_folder, name, value)
%
% Inputs:
%   base_folder - Folder to scan for files
%
% name, value arguments:
%   extensions - list of file extensions to search for. Defaults to
%                [".ary", ".aryx"]
%   recursive  - specify if the search should be done recursive, searching
%                all sub folders as well. Default: false
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

    arguments
        base_folder string {mustBeTextScalar, mustBeFolder}
        opts.extensions string {mustBeText} = [".ary", ".aryx"]
        opts.recursive (1,1) logical = false
    end
    file_list = strings(0);
    file_list = recurse_folder(string(base_folder), file_list, opts);
end

function all_files = recurse_folder(folder, all_files, opts)
    file_list = dir(folder);
    ind_dirs = [file_list.isdir];
    file_names = convertCharsToStrings({file_list.name})';
    [~,~,ext] = fileparts(file_names);
    ind_spectra = arrayfun(@(e) any(strcmpi(e, opts.extensions)), ext);
    new_files = fullfile(folder, file_names(ind_spectra));
    all_files = vertcat(all_files, new_files);
    if opts.recursive
        dir_names = file_names(ind_dirs);
        for iPath = 1:numel(dir_names)
            if ~(strcmp(dir_names(iPath), "..") || strcmp(dir_names(iPath), "."))
                all_files = recurse_folder(fullfile(folder, dir_names(iPath)), all_files, opts);
            end
        end
    end
end
