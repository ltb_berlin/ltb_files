function write_ltb_aryx(filename, y, x, o, metadata)
%WRITE_LTB_ARYX Write spectra data to an aryx file
% SYNOPSIS
%   write_ltb_aryx(filename, y, x, o, metadata)
% INPUTS
%   filename: The name of the file to be written
%   y: column vector of intensity values
%   x: column vector of wavelength values
%   o: column vector of spectral orders
%   metadata: table of metadata
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
arguments
    filename (1,1) string {mustBeTextScalar, validators.mustBeLegalPath}
    y (:,1) double
    x (:,1) double {validators.mustBeEqualSize(x, y), validators.mustBeMonotonic}
    o (:,1) double {validators.mustBeEqualSize(o, y), mustBeInteger, mustBePositive}
    metadata {mustBeA(metadata, "table"), validators.mustHaveRowsForColumns(metadata, y)}
end
    temp_folder = strcat("unzipped-", dec2hex(randi(16,1,32)-1)');
    full_folder = fullfile(tempdir, temp_folder);
    % remove possible old unzipped folder
    if exist(full_folder, "dir"), rmdir(full_folder, "s"); end
    mkdir(full_folder);
    cleanup_folder = onCleanup(@() rmdir(full_folder, "s"));
    [~, name, ext] = fileparts(filename);
    
    if strcmpi(metadata.x_name, "Raman shift") && ...
            ismember("ramanExcitationWavelength", metadata.Properties.VariableNames) && ... % Protection against legacy data
            (metadata.ramanExcitationWavelength > 0)
        x = (1e7 ./ ((1e7 / metadata.ramanExcitationWavelength) - x));
    end

    [o, ind] = sort(o, "descend");
    x = x(ind);
    y = y(ind);

    spec_file = fullfile(full_folder, strcat(name, ".~tmp"));
    write_values(spec_file, y, x);

    aif_file = fullfile(full_folder, strcat(name, ".~aif"));
    write_aif(aif_file, x, o);

    assert(ismember("version", metadata.Properties.VariableNames), "ltbfiles:write_ltb_aryx:bad_metadata", ...
        "Metadata must not be legacy");
    meta_file = fullfile(full_folder, "metadata_summary.json");
    write_meta_versioned(meta_file, metadata);

    zip(filename, [spec_file, aif_file, meta_file], "");
    if ~strcmp(ext, ".zip")
        movefile(strcat(filename, ".zip"), filename);
    end
end

function write_values(spec_file, y, x)
    fid = fopen(spec_file, "w");
    cleanup = onCleanup(@() fclose(fid));
    fwrite(fid, [y,x]', "double", 0, "l");
end

function write_aif(aif_file, x, o)
    orders = unique(o);
    fid = fopen(aif_file, "w");
    cleanup = onCleanup(@() fclose(fid));
    for i_order = 1:numel(orders)
        ind_order = o == orders(i_order);
        first_pix = find(ind_order, 1, "first");
        last_pix = find(ind_order, 1, "last");
        fwrite(fid, first_pix-1, ltbfiles.FileConstants.ARYX_AIF_TYPES(1), 0, "l");
        fwrite(fid, last_pix-1, ltbfiles.FileConstants.ARYX_AIF_TYPES(2), 0, "l");
        fwrite(fid, orders(i_order), ltbfiles.FileConstants.ARYX_AIF_TYPES(3), 0, "l");
        fwrite(fid, 0, ltbfiles.FileConstants.ARYX_AIF_TYPES(4), 0, "l"); % raw image column start, can not be recovered -> fake
        fwrite(fid, last_pix - first_pix, ltbfiles.FileConstants.ARYX_AIF_TYPES(5), 0, "l"); % raw image column end, can not be recovered -> fake
        fwrite(fid, 0, ltbfiles.FileConstants.ARYX_AIF_TYPES(6), 0, "l"); % filler, always 0
        fwrite(fid, x(first_pix), ltbfiles.FileConstants.ARYX_AIF_TYPES(7), 0, "l");
        fwrite(fid, x(last_pix), ltbfiles.FileConstants.ARYX_AIF_TYPES(8), 0, "l");
    end
end

function write_meta_versioned(meta_file, metadata)
    switch metadata.version
        case 1
            metadata.timeStampMeasurement = string(metadata.timestamp, "yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            json = jsonencode( ...
                table2struct(metadata(1,ltbfiles.FileConstants.METADATA_HEADERS_V1)), ...
                ConvertInfAndNaN=true, PrettyPrint=true);
            % Works only since R2023b
            %     writestruct(table2struct(metadata(1,ltbfiles.FileConstants.METADATA_HEADERS_V1), ...
            %         meta_file, FileType="json"); 

            fid = fopen(meta_file, "w");
            cleanup = onCleanup(@() fclose(fid));
            fwrite(fid, unicode2native(json, "UTF-8"));
        otherwise
            error("ltbfiles:write_ltb_aryx:write_metadata_versioned:unknown_version", ...
                "Metadata version %d is unknown", metadata.version);
    end
end
