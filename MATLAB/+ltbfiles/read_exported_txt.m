function [y, x, head] = read_exported_txt(filename)
%READ_EXPORTED_TXT Read *.txt files the way they are exported from Sophi_nXt.
%
% This function is provided for completeness only. It is not recommended to export spectra into *.txt files. Use the
% native *.aryx format and the provided loading functions instead.
%
% Syntax: [spec, head] = read_exported_txt(filename)
%
% Inputs:
%   filename - Name of the file to be loaded.
%
% Outputs:
%   y    - The spectrum loaded
%   x    - The wavelengths
%   head - Some metadata
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

arguments
    filename (1,1) string {mustBeTextScalar, mustBeFile}
end
content = readmatrix(filename);
assert(size(content, 2) == 2, "ltbfiles:read_exported_txt", "File must contain two columns of data");
x = content(:,1);
y = content(:,2);
[XPos, YPos] = ltbfiles.extract_scan_position(filename);
head = cell2table(ltbfiles.FileConstants.METADATA_DEFAULT_V1, VariableNames=ltbfiles.FileConstants.METADATA_HEADERS_V1);
head.posX = XPos;
head.posY = YPos;
head.filename = filename;
head.timestamp = NaT(1, TimeZone="UTC");
head.x_name = "Wavelength";
head.x_unit = "nm";
