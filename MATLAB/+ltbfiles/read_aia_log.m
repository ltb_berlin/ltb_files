function log = read_aia_log(filename)
%READ_AIA_LOG Read auto image alignment log files
% SYNOPSIS
%   log = read_aia_log(filename)
% INPUTS
%   filename: Name of the log file to be read
% OUTPUTS
%   log: Table of auto image alignment results
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

arguments
    filename (1,1) string {mustBeText, mustBeFile}
end
log = readtable(filename, Delimiter=";", DatetimeType="text", TextType="string");
times = datetime(log.TimeStamp, InputFormat="yyyy-MM-dd'T'HH:mm:ssXXX", TimeZone="UTC");
indNaT = isnat(times);
times(indNaT) = datetime(log.TimeStamp(indNaT), TimeZone="local");
log.TimeStamp = times;
