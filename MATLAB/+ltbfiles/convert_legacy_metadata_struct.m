function metadata = convert_legacy_metadata_struct(head)
%CONVERT_LEGACY_METADATA_STRUCT Convert legacy metadata read from ".~json" files into the new format used in summary.json
% SYNOPSIS
%   metadata = convert_legacy_metadata_aryx(head)
% INPUTS
%   head: The json decoded metadata, passed as struct
% OUTPUTS
%   metadata: A metadata table matching the requirements of a summary.json
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

arguments
    head {mustBeA(head, "struct")}
end
assert(isfield(head, "measure"), "ltbfiles:read_ltb_aryx:metadata_missing", "'measure' metadata is missing");
try
    if isfield(head.measure, "ISOFormat")
        time_str = string(head.measure.ISOFormat);
    else
        tmp_str = string(strcat(head.measure.Date, " " , head.measure.TimeStamp));
        timestamp = datetime(tmp_str, InputFormat="yyyy-MM-dd HH:mm:ss.SSS", TimeZone="local");
        timestamp.TimeZone = "UTC";
        time_str = string(timestamp, ltbfiles.FileConstants.TIMESTAMP_FORMAT);
    end
catch
    time_str = ltbfiles.FileConstants.TIMESTAMP_MISSING;
end
if ~isfield(head, "libsControl")
    warning("ltbfiles:read_ltb_aryx:metadata_missing", "'libsControl' metadata is missing");
end
if ~isfield(head, "detector")
    warning("ltbfiles:read_ltb_aryx:metadata_missing", "'detector' metadata is missing");
end
gf = @tools.get_field;
metadata = table(1, "", string(gf(head.measure, "SerialNumber", "")), string(gf(head.measure, "SetupName", "")), "", ...
    ltbfiles.FileConstants.TIMESTAMP_MISSING, time_str, gf(head.measure, "ExcitationLength"), "AtRest", gf(head.measure, "XPos"), ...
    gf(head.measure, "YPos"), gf(head.measure, "ZPos"), NaN, NaN, NaN, ...
    "", "", "", NaN, gf(head, "detector.ExposureTime"), ...
    string(gf(head, "detector.Shutter.Mode", "Open")), false, NaN, NaN, NaN, ...
    0, ltbfiles.FileConstants.TIMESTAMP_MISSING, gf(head, "detector.Binning.Horizontal"), gf(head, "detector.Binning.Vertical"), gf(head, "detector.HorizontalShiftSpeed"), ...
    gf(head, "detector.GainMCP"), gf(head, "detector.GainEMCCD"), gf(head, "libsControl.ExperimentalDelay"), gf(head, "libsControl.FlashLampQSwitchDelay"), gf(head, "libsControl.FlashLampQSwitchDelay2"), ...
    gf(head, "libsControl.InterpulseDelay"), gf(head, "detector.ICCDGateWidth"), NaN, NaN, NaN, ...
    gf(head, "libsControl.LaserFrequency"), NaN, gf(head, "libsControl.Laser1CleaningShots"), gf(head, "detector.AverageCount"), gf(head, "detector.SubtractDarkImage", true), ...
    gf(head, "detector.Temperature"), NaN, NaN, NaN, NaN, ...
    VariableNames=ltbfiles.FileConstants.METADATA_HEADERS_V1);
