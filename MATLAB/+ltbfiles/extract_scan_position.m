function [xpos, ypos] = extract_scan_position(filename, opts)
%EXTRACT_SCAN_POSITION Extract the location of a measurement from the filename
%   [xpos, ypos] = extract_scan_position(filename, name,value)
%   
%   Extracts x and y coordinate from the filename string
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

arguments
    filename (1,1) string {mustBeTextScalar, mustBeFile}
    opts.expression (1,1) string {mustBeTextScalar} = ".*[xX][Pp]os(?:ition)?_(-?\d+)_[yY][Pp]os(?:ition)?_(-?\d+)"
end
pos = regexp(filename, opts.expression, "tokens", "once");
if isempty(pos)
    xpos = NaN;
    ypos = NaN;
else
    xpos = str2double(pos{1});
    ypos = str2double(pos{2});
end
