function [image, params] = read_ltb_rawx(filename)
%read_ltb_rawx Open an LTB-rawx file as echellogram image and parameters of the measurement.
%
% Syntax: [image,params]=read_ltb_rawx(filename)
%
% Inputs:
%    filename - Name of the file to be loaded
%
% Outputs:
%    image  - 2D array containing the echellogram as an image
%    params - structure containing all measurement parameters
%
% Requirement:
%    Requires the function ReadIniStruct.m in order to load the measurement
%    parameters.
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

    arguments
        filename string {mustBeTextScalar, mustBeFile}
    end

    assert(~isempty(tempdir), "read_ltb_rawx:temp_not_found", "Could not find temp path");
    
    % Avoid collision with other instances accessing the same folder by using
    % UUID for unzip folders
    % http://www.briandalessandro.com/blog/how-to-create-a-random-uuid-in-matlab/
    % for compatibility with Octave:
    % http://codegolf.stackexchange.com/questions/58442/generate-random-uuid/58761
    extractFolder = strcat("unzipped-", dec2hex(randi(16,1,32)-1)');
    full_folder = fullfile(tempdir, extractFolder);
    %remove possible old unzipped folder 
    if exist(full_folder, "dir"), rmdir(full_folder, "s"); end
    
    %extract (will raise an error if target dir is write protected)
    files = unzip(filename,fullfile(tempdir,extractFolder));
    cleanup = onCleanup(@() rmdir(full_folder, "s"));
    [path, names, exts] = fileparts(files);
    
    ind=strcmpi(".rawdata", exts);
    UnZippedFile = fullfile(path{ind}, strcat(names{ind}, ".rawdata"));
    values = load_values(UnZippedFile);
    
    params = read_params(path, names);
    
    image = reshape(values, str2double(params.dongle.CCD.Width)/str2double(params.sophi.Echelle1.vertical_binning), [])';
end

function values = load_values(file)
    fid=fopen(file, "r");
    cleanup = onCleanup(@() fclose(fid));
    values = fscanf(fid, '%i');
end

function params = read_params(paths, filenames)
    ind = strcmpi("Aryelle", filenames);
    UnZippedFile = fullfile(paths{ind}, "Aryelle.ini");
    paramsAry = ReadIniStruct(UnZippedFile);
    ind = strcmpi('Sophi', filenames);
    UnZippedFile = fullfile(paths{ind}, "Sophi.ini");
    paramsSophi = ReadIniStruct(UnZippedFile);
    params = struct(dongle=paramsAry, sophi=paramsSophi);
end

function content=ReadIniStruct(filename)
    % Read the contents of an *.ini-file into a structure
    fid=fopen(filename);
    cleanup = onCleanup(@() fclose(fid));
    curr_section="";
    content=[];
    while ~feof(fid)
        str=fgetl(fid);
        if  ~isempty(str)
            if strcmp("[",str(1))
                curr_section=strjoin(regexp(str, "[\w*]", "match"), '');
            elseif ~strcmp(";", str(1))
                values=textscan(str,"%s %s",delimiter="=");
                field_name=strrep(string(values{1}), " ", "_");
                field_name=strrep(field_name,"-","__");
                content.(curr_section).(field_name)=string(values{2});
            end
        end
    end
end
