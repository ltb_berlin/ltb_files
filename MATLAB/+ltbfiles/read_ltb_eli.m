function [spec, head] = read_ltb_eli(filename)
%READ_LTB_ELI Read *.eli files created by a wavemeter from LTB.
%
% Syntax: [spec, head] = read_ltb_eli(filename)
%
% Inputs:
%   filename - Name of the file to be loaded.
%
% Outputs:
%   spec - The spectrum loaded
%   head - The header information
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

arguments
    filename (1,1) string {mustBeTextScalar, mustBeFile}
end
content = readlines(filename, EmptyLineRule="read");
[head, iLine] = read_head_eli_ess(content);
assert(isfield(head, "width"), "ltbfiles:read_ltb_eli:width_missing", "Could not identify with");
spec = nan(head.width, 1);
iSpecLine = 1;
for iLine = iLine + 1 : iLine + head.width
    spec(iSpecLine) = str2double(content(iLine));
    iSpecLine = iSpecLine + 1;
end
head.timestamp = datetime(strcat(content(iLine + 1), " ", content(iLine + 2)), InputFormat="MM/dd/yyyy HH:mm:ss.SSS", TimeZone="local");
head.serial_number = content(iLine + 3);
