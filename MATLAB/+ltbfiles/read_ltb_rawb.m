function [image,additional] = read_ltb_rawb(filename)
% read_ltb_rawb Open an LTB-rawb file as echellogram image.
%   [image, additional] = read_ltb_rawb(filename) read an image file and
%   return a 2D array (image) and additional information about the sensor
%   and binning settings.
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
arguments
    filename string {mustBeTextScalar, mustBeFile}
end
fid=fopen(filename, 'rb');
cleanup = onCleanup(@() fclose(fid));
additional= struct('ChipWidth',   fread(fid, 1, 'uint'),...
                   'ChipHeight',  fread(fid, 1, 'uint'),...
                   'PixelSize',   fread(fid, 1, 'double'),...
                   'HorBinning',  fread(fid, 1, 'uint'),...
                   'VerBinning',  fread(fid, 1, 'uint'),...
                   'BottomOffset',fread(fid, 1, 'uint'),...
                   'LeftOffset',  fread(fid, 1, 'uint'),...
                   'ImgHeight',   fread(fid, 1, 'uint'),...
                   'ImgWidth',    fread(fid, 1, 'uint'));
B=fread(fid, Inf, 'int32');
image=reshape(B,additional.ImgWidth,additional.ImgHeight)';
