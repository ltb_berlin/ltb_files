function [Y, x, o, metadata] = load_files(filenames, opts)
%LOAD_FILES This function loads multiple *.ary or *.aryx files into a combined matrix.
%
% Syntax: [Y, x, o, metadata] = LOAD_FILES(filenames)
%
% Inputs:
%   filenames - List of files to be loaded. May be relative paths, files in
%               the current workspace or fully qualified filenames
%               including paths. In the latter case, files can be loaded
%               from different locations.
% name, value arguments:
%    interpolate - Specify if the function should interpolate spectra if
%                  necessary. Will fail otherwise.
%
% Outputs:
%   Y - Matrix of intensity values. Spectra are ordered column wise.
%   x - Unified wavelength axis for all spectra
%   o - Unified order list
%   metadata - measurement settings and other information stored in the
%   files
%
% If the wavelength axis x differs across the spectra, the name-value argument
% 'interpolate' is used to decide if interpolation should be done or if the
% function should raise an exception.
%
%   >>> LICENSE
%   Copyright (C) 2024 Dr. Sven Merk
%   sven.merk@ltb-berlin.de
%
%   This program is free software; you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation; either version 3 of the License, or
%   (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License along
%   with this program; if not, write to the Free Software Foundation, Inc.,
%   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    arguments
        filenames string {mustBeText, mustBeFile, mustBeNonzeroLengthText, mustBeNonempty}
        opts.interpolate (1,1) logical = false
    end
    n_spec = numel(filenames);
    [y, x, o, h] = load_file(filenames(1));
    Y = nan(size(y,1), n_spec);
    Y(:,1) = y;
    metadata = table(size=[n_spec, width(h)], ...
        VariableTypes=varfun(@class, h, OutputFormat="cell"), ...
        VariableNames=h.Properties.VariableNames);
    metadata.timestamp = NaT(n_spec, 1, TimeZone="UTC");
    metadata(1,:) = h;
    if n_spec > 1
        for iSpec = n_spec:-1:2
            curr_file = filenames(iSpec);
            [y, wl, or, h] = load_file(curr_file);
            if ~(isequal(x, wl) && isequal(o, or))
                assert(opts.interpolate, "ltbfiles:load_files:interpolation_required", "The files require interpolation. Consider setting 'interpolate' to true");
                y = interp1(wl, y, x);
                o = zeros(size(x));
            end
            Y(:,iSpec) = y;
            metadata(iSpec,:) = h;
        end
    end
end

function [y, wl, order, h] = load_file(file)
    [~,~,ext] = fileparts(file);
    switch ext
        case ".ary"
            [y,wl,order,h] = ltbfiles.read_ltb_ary(file);
        case ".aryx"
            [y,wl,order,h] = ltbfiles.read_ltb_aryx(file);
        otherwise
            error("ltbfiles:load_files:unknown_extension", "%s is not a known extension", ext);
    end
end
