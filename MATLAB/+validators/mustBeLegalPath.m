function mustBeLegalPath(path)
% Assert that a given folder or filename is a legal path on the current platform. The file or folder does not need to exist.
if ~tools.is_legal_path(path)
    throwAsCaller(MException("validators:notLegalPath", """%s"" is not a legal path name", path));
end
