function mustBeEqualSize(a,b)
% Assert thet both arrays have the same dimensions
if ~isequal(size(a), size(b))
    throwAsCaller(MException("validators:notMatchingSize", "Both arrays must have the same size"));
end

