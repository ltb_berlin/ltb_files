function mustBeMonotonic(vals, opts)
%MUSTBEMONOTONIC Assert that the values of a vector are increasing or decreasinf monotonically
% SYNOIPSIS
%   mustBeMonotonic(vals, name,value)
% INPUTS
%   vals: vector of values to be checked
% NAME-VALUE-ARGUMENTS
%   direction: Optional ("increase") - Either "increase" or "decrease", specifying if the values must be monotonically
%       increasing or decreasing
arguments
    vals (:,1) double
    opts.direction (1,1) string {mustBeTextScalar, mustBeMember(opts.direction, ["increase", "decrease"])} = "increase"
end
diffs = diff(vals);
if strcmp(opts.direction, "increase")
    if any(diffs <= 0)
        throwAsCaller(MException("validators:not_monotonically_increasing", "Must be increasing monotonically"));
    end
else
    if any(diffs >= 0)
        throwAsCaller(MException("validators:not_monotonically_decreasing", "Must be decreasing monotonically"));
    end
end
