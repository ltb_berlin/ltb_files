function mustHaveRowsForColumns(in,ref)
% Assert that in has the same number of rows as ref has columns
if ~isequal(size(in, 1), size(ref,2))
    throwAsCaller(MException("validators:notMatchingToReference", "Argument needs one row for each reference column"));
end
