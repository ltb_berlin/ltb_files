import io
import tempfile
from dateutil import parser
import pytest
import numpy as np
import ltbfiles


@pytest.mark.parametrize('sort', (True, False))
def test_load_ary(ary_file, sort):
    y,x,o,head = ltbfiles.read_ltb_ary(ary_file, sort_wl=sort)
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(head.keys())
    assert len(y) == len(x)
    assert len(y) == len(o)
    assert head['filename'] == ary_file
    assert head["x_name"] == "Wavelength"
    x_sorted = np.sort(x)
    assert sort == (x_sorted == x).all()


def test_load_ary_opened(ary_file):
    with open(ary_file, "rb") as fid:
        y,x,o,head = ltbfiles.read_ltb_ary(fid)
    assert len(y) == len(x)
    assert len(y) == len(o)
    assert head['filename'] == ary_file
    assert head["x_name"] == "Wavelength"


def test_load_ary_stream(ary_file):
    with open(ary_file, "rb") as fid:
        content = io.BytesIO(fid.read())
    y,x,o,head = ltbfiles.read_ltb_ary(content)
    assert len(y) == len(x)
    assert len(y) == len(o)
    assert head['filename'] == "memory"
    assert head["x_name"] == "Wavelength"


@pytest.mark.parametrize("ary_file", ("ary_raman532", "ary_raman785"))
def test_load_ary_raman(ary_file, request):
    y,x,_,head = ltbfiles.read_ltb_ary(request.getfixturevalue(ary_file))
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(head.keys())
    assert head["x_name"] == "Raman shift"
    i_stokes = np.where(np.logical_and(x > 80, x < 1400))[0]
    i_max = np.argmax(y[i_stokes])
    x_max = x[i_stokes[i_max]]
    assert x_max == pytest.approx(521, abs=1)


def test_load_ary_docstr():
    docstr = ltbfiles.read_ltb_ary.__doc__
    assert docstr is not None
    assert "Read data from a binary *.ary file" in docstr


def test_load_spectra_is_object(ary_file):
    result = ltbfiles.read_ltb_ary(ary_file)
    assert isinstance(result, ltbfiles.Spectra)
    assert isinstance(result.Y, np.ndarray)
    assert isinstance(result.o, np.ndarray)
    assert isinstance(result.x, np.ndarray)
    assert isinstance(result.head, dict)


@pytest.mark.parametrize("file", ["ary_missing_rep", "ary_empty_rep"])
def test_load_ary_corrupted(file, request):
    with pytest.raises(ltbfiles.CorruptedFileException):
        ltbfiles.read_ltb_ary(request.getfixturevalue(file))


@pytest.mark.parametrize('sort', (True, False))
def test_load_aryx(spectra_dir, sort):
    filename = spectra_dir / "noise.aryx"
    y,x,o,head = ltbfiles.read_ltb_aryx(filename, sort_wl=sort)
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(head.keys())
    assert len(y) == len(x)
    assert len(y) == len(o)
    assert head['filename'] == filename
    assert head["x_name"] == "Wavelength"
    x_sorted = np.sort(x)
    assert sort == (x_sorted == x).all()


def test_load_aryx_iccd(aryx_iccd):
    _,_,_,head = ltbfiles.read_ltb_aryx(aryx_iccd)
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(head.keys())
    assert head["gainMCP"] > 0
    assert head["gateWidthMCP"] > 0


def test_load_aryx_meta_v1(aryx_meta_v1):
    _,_,_,head = ltbfiles.read_ltb_aryx(aryx_meta_v1)
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(head.keys())
    assert isinstance(head["sessionGUID"], str)
    assert isinstance(head["posX"], float)
    assert np.isnan(head["posA"])
    kernel_time = parser.isoparse(head["timeStampKernelStart"])
    assert kernel_time.year == 2024
    aia_time = parser.isoparse(head["aiaTimeStamp"])
    assert aia_time.year == 2023


def test_load_aryx_meta_v1_missing_timestamps(aryx_meta_v1_missing_timestamps):
    _,_,_,head = ltbfiles.read_ltb_aryx(aryx_meta_v1_missing_timestamps)
    assert head["timeStampKernelStart"] == ""
    assert head["aiaTimeStamp"] == ""


@pytest.mark.parametrize("file", ["aryx_metadata_bad_version", "aryx_metadata_missing", "aryx_metadata_empty"])
def test_load_aryx_corrupted(file, request):
    with pytest.raises(ltbfiles.CorruptedFileException):
        ltbfiles.read_ltb_aryx(request.getfixturevalue(file))


def test_write_aryx_stream(aryx_with_iso_and_coord):
    spec = ltbfiles.read_ltb_aryx(aryx_with_iso_and_coord)
    out = io.BytesIO()
    ltbfiles.write_ltb_aryx(out, spec)
    spec_load = ltbfiles.read_ltb_aryx(out)
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(spec_load.head.keys())
    assert np.array_equal(spec.x, spec_load.x)
    assert np.array_equal(spec.Y, spec_load.Y)
    assert np.array_equal(spec.o, spec_load.o)


def test_write_aryx_file(aryx_with_iso_and_coord, tmp_file):
    spec = ltbfiles.read_ltb_aryx(aryx_with_iso_and_coord)
    ltbfiles.write_ltb_aryx(tmp_file, spec)
    spec_load = ltbfiles.read_ltb_aryx(tmp_file)
    assert np.array_equal(spec.x, spec_load.x)
    assert np.array_equal(spec.Y, spec_load.Y)
    assert np.array_equal(spec.o, spec_load.o)


def test_write_aryx_file_handle(aryx_with_iso_and_coord):
    spec = ltbfiles.read_ltb_aryx(aryx_with_iso_and_coord)
    with tempfile.NamedTemporaryFile(mode="w+b") as fid:
        ltbfiles.write_ltb_aryx(fid, spec)
        fid.seek(0)
        spec_load = ltbfiles.read_ltb_aryx(fid)
    assert np.array_equal(spec.x, spec_load.x)
    assert np.array_equal(spec.Y, spec_load.Y)
    assert np.array_equal(spec.o, spec_load.o)


def test_write_aryx_fail(spectra_dir):
    spec = ltbfiles.load_folder(spectra_dir, extensions=[".aryx"])
    assert spec is not None
    with pytest.raises(ValueError):
        ltbfiles.write_ltb_aryx(io.BytesIO(), spec)


@pytest.mark.parametrize("aryx_file", ("aryx_raman532", "aryx_raman785"))
def test_load_aryx_raman(aryx_file, request):
    y,x,_,head = ltbfiles.read_ltb_aryx(request.getfixturevalue(aryx_file))
    assert (ltbfiles.METADATA_HEADERS_V1 + ltbfiles.METADATA_ADDED) == tuple(head.keys())
    assert head["x_name"] == "Raman shift"
    i_stokes = np.where(np.logical_and(x > 80, x < 1400))[0]
    i_max = np.argmax(y[i_stokes])
    x_max = x[i_stokes[i_max]]
    assert x_max == pytest.approx(521, abs=1)

def test_load_aryx_docstr():
    docstr = ltbfiles.read_ltb_aryx.__doc__
    assert docstr is not None
    assert "Read data from a binary *.aryx file" in docstr


@pytest.mark.parametrize('file', ('steel.ary','noise.aryx'))
def test_load_multi_spec(spectra_dir, file):
    filenames = [spectra_dir / file] * 2
    data, wl, o, head = ltbfiles.load_files(filenames)
    assert 2 == data.shape[1]
    assert 1 == len(wl.shape)
    assert 1 == len(o.shape)
    assert data.shape[0] == len(wl)
    assert 2 == len(head)
    increasing = all(x1<x2 for x1, x2 in zip(wl, wl[1:]))
    assert increasing
    names_recovered = [part['filename'] for part in head]
    assert names_recovered == filenames


def test_load_files_interpolation(spectra_dir):
    files = ltbfiles.scan_for_files(spectra_dir)
    with pytest.raises(ltbfiles.IncompatibleSpectraException) as exc:
        ltbfiles.load_files(files)
    assert "spectra_basic" in exc.value.args[0]
    spec = ltbfiles.load_files(files, interpolate=True)
    assert spec.Y.shape[1] == len(files)


@pytest.mark.parametrize('file', ('steel.ary','noise.aryx'))
def test_load_files_single(spectra_dir, file):
    filenames = spectra_dir / file
    data, wl, o, head = ltbfiles.load_files([filenames])
    assert 1 == data.shape[1]
    assert 1 == len(wl.shape)
    assert 1 == len(o.shape)
    assert data.shape[0] == len(wl)
    assert 1 == len(head)
    names_recovered = [part['filename'] for part in head]
    assert names_recovered[0] == filenames


def test_load_files_docstr():
    docstr = ltbfiles.load_files.__doc__
    assert docstr is not None
    assert "Read the content of multiple spectra files" in docstr


@pytest.mark.parametrize("file", [["foo.bar"], "foo.bar"])
def test_load_files_wrong_type(file):
    with pytest.raises(Exception) as excinfo:
        ltbfiles.load_files(file)
    assert "Unknown file extension '.bar'" == excinfo.value.args[0]


def test_find_spectra_in_folder(spectra_dir):
    filelist = ltbfiles.scan_for_files(spectra_dir)
    assert 3 == len(filelist)


def test_find_spectra_in_folder_selected(spectra_dir):
    filelist = ltbfiles.scan_for_files(spectra_dir, extensions='.aryx')
    assert 2 == len(filelist)


@pytest.mark.parametrize(('ext','num'), [('.ary',1), ('.aryx',2)])
def test_load_folder(spectra_dir, ext, num):
    loaded = ltbfiles.load_folder(spectra_dir, extensions=[ext])
    assert loaded is not None
    assert isinstance(loaded, ltbfiles.Spectra)
    assert isinstance(loaded.Y, np.ndarray)
    assert loaded.Y.shape[1] == num
    assert len(loaded.head) == num
    assert isinstance(loaded.head[0], dict)


def test_load_folder_no_spec(spectra_dir):
    loaded = ltbfiles.load_folder(spectra_dir, extensions=['.foo'])
    assert loaded is None


def test_load_non_matching_folder(spectra_dir):
    with pytest.raises(Exception) as excinfo:
        ltbfiles.load_folder(spectra_dir)
    assert str(spectra_dir) in excinfo.value.args[0]


def test_load_folder_interpolation(spectra_dir):
    with pytest.raises(ltbfiles.IncompatibleSpectraException) as exc:
        ltbfiles.load_folder(spectra_dir)
    assert "spectra_basic" in exc.value.args[0]
    spec = ltbfiles.load_folder(spectra_dir, interpolate=True)
    assert spec is not None
    assert spec.Y.shape[1] == len(ltbfiles.scan_for_files(spectra_dir))


def test_load_raw(raw_file):
    img, head = ltbfiles.read_ltb_raw(raw_file)
    assert (512, 2048) == img.shape
    assert isinstance(head, dict)
    assert 9 == len(head)


def test_load_rawb(rawb_file):
    img, head = ltbfiles.read_ltb_rawb(rawb_file)
    assert (512, 2048) == img.shape
    assert isinstance(head, dict)
    assert 9 == len(head)


def test_load_rawx(rawx_file):
    img, head = ltbfiles.read_ltb_rawx(rawx_file)
    assert (512, 2048) == img.shape
    assert isinstance(head, dict)
    assert 2 == len(head)
