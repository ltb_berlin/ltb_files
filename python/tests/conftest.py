from pathlib import Path
import tempfile
import pytest


@pytest.fixture(name="tmp_file")
def fixture_tmp_file(request):
    ext = None
    if hasattr(request, 'param'):
        ext = request.param.get("extension")
    try:
        with tempfile.NamedTemporaryFile(delete=False, prefix="test_", suffix=ext) as fid:
            tmp_name = Path(fid.name)
            yield tmp_name
    finally:
        tmp_name.unlink()

@pytest.fixture(name="files_dir")
def fixture_files_dir():
    return Path(__file__).parent.parent.parent / 'test_files'


@pytest.fixture(name="spectra_dir")
def fixture_spectra_dir(files_dir):
    return files_dir / "spectra_basic"


@pytest.fixture(name="ary_file")
def fixture_ary_file(spectra_dir):
    return spectra_dir / 'steel.ary'

@pytest.fixture(name="spectra_corrupted")
def fixture_spectra_corrupted(files_dir):
    return files_dir / "corrupted"

@pytest.fixture(name="ary_empty_rep")
def fixture_ary_empty_rep(spectra_corrupted):
    return spectra_corrupted / "al_empty_rep.ary"

@pytest.fixture(name="ary_missing_rep")
def fixture_ary_missing_rep(spectra_corrupted):
    return spectra_corrupted / "al_missing_rep.ary"

@pytest.fixture(name="raman_dir")
def fixture_raman_dir(files_dir):
    return files_dir / "raman"

@pytest.fixture(name="ary_raman532")
def fixture_ary_raman532(raman_dir):
    return raman_dir / "Si_R532.ary"

@pytest.fixture(name="ary_raman785")
def fixture_ary_raman785(raman_dir):
    return raman_dir / "Si_R785.ary"

@pytest.fixture(name="aryx_raman532")
def fixture_aryx_raman532(raman_dir):
    return raman_dir / "Si_R532.aryx"

@pytest.fixture(name="aryx_raman785")
def fixture_aryx_raman785(raman_dir):
    return raman_dir / "Si_R785.aryx"

@pytest.fixture(name="aryx_file")
def fixture_aryx_file(spectra_dir):
    return spectra_dir / 'noise.aryx'

@pytest.fixture(name="aryx_metadata_missing")
def fixture_aryx_metadata_missing(spectra_corrupted):
    return spectra_corrupted / "metadata_missing.aryx"

@pytest.fixture(name="aryx_metadata_incomplete")
def fixture_aryx_metadata_incomplete(spectra_corrupted):
    return spectra_corrupted / "metadata_incomplete.aryx"

@pytest.fixture(name="aryx_metadata_empty")
def fixture_aryx_metadata_empty(spectra_corrupted):
    return spectra_corrupted / "metadata_empty.aryx"

@pytest.fixture(name="aryx_metadata_bad_version")
def fixture_aryx_metadata_bad_version(spectra_corrupted):
    return spectra_corrupted / "bad_summary_version.aryx"

@pytest.fixture(name="spectra_advanced")
def fixture_spectra_advanced(files_dir):
    return files_dir / "spectra_advanced"

@pytest.fixture(name="aryx_with_iso_and_coord")
def fixture_aryx_with_iso_and_coord(spectra_advanced):
    return spectra_advanced / "isotime_and_coords.aryx"

@pytest.fixture(name="aryx_iccd")
def fixture_aryx_iccd(spectra_advanced):
    return spectra_advanced / "iccd.aryx"

@pytest.fixture(name="aryx_meta_v1")
def fixture_aryx_meta_v1(spectra_advanced):
    return spectra_advanced / "with_summary_v1.aryx"

@pytest.fixture(name="aryx_meta_v1_missing_timestamps")
def fixture_aryx_meta_v1_missing_timestamps(spectra_advanced):
    return spectra_advanced / "with_summary_v1_timestamps_missing.aryx"

@pytest.fixture(name="raw_image_folder")
def fixture_raw_folder(files_dir):
    return files_dir / "raw_images"


@pytest.fixture(name="raw_file")
def fixture_raw_file(raw_image_folder):
    return raw_image_folder / "deuterium.raw"


@pytest.fixture(name="rawb_file")
def fixture_rawb_file(raw_image_folder):
    return raw_image_folder / "deuterium.rawb"


@pytest.fixture(name="rawx_file")
def fixture_rawx_file(raw_image_folder):
    return raw_image_folder / "deuterium.rawx"


@pytest.fixture(name="aia_folder")
def fixture_aia_folder(files_dir):
    return files_dir / "aia_log"


@pytest.fixture(name="aia_file_csv")
def fixture_aia_file_csv(aia_folder):
    return aia_folder / "AutoImage.csv"


@pytest.fixture(name="aia_file_log")
def fixture_aia_file_log(aia_folder):
    return aia_folder / "AutoImage.log"
