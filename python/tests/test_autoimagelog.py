# pylint: disable=missing-function-docstring, missing-module-docstring
from ltbfiles import autoimagelog


def test_read_aia_csv(aia_file_csv):
    aia_data = autoimagelog.read_aia_csv(aia_file_csv)
    assert (85,5) == aia_data.shape
    assert aia_data.index[83].day == 10
    assert aia_data.index[84].day == 10
    assert aia_data.index[84].hour == 14


def test_read_aia_log(aia_file_log):
    aia_data = autoimagelog.read_aia_log(aia_file_log)
    assert aia_data.index[0].day == 16
    assert aia_data.index[0].hour == 16
