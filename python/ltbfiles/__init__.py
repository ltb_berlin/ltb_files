"""
Python package for loading files created with Spectrometers from LTB Lasertechnik Berlin GmbH.

.. include:: ../../README.md
.. include:: ../../doc/format_overview.md
"""
from .ltbfiles import *
__version__ = "4.1.1"
__author__ = "Sven Merk"
