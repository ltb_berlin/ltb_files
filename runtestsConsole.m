backend_folder = "MATLAB";
addpath(backend_folder);
try    
    res = tests.runtests(backend_folder);
    pause(0.300);
    exit(any([res.Failed]));
catch e
    disp(e.getReport);
    exit(2);
end