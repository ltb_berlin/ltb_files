# LTB-File formats

The file formats can be grouped into files for spectra (1D-Data) and raw images (echellograms, 2D-Data).

## Spectra

- [*.ary-Files](#ary-and-aryx-file-format), legacy format, can be created with all Sophi-Versions
- *.aryx-Files, new format, can be created with Sophi_nXt (8 and later)

### ARY and ARYX-File Format

#### General description

ARYX files (and the legacy ARY) are an efficient way to store spectra measured with an echelle spectrometer. They are the recommended format for preprocessed (not raw) spectra generated with the software Sophi from LTB.

#### File content

In addition to the intensities and the wavelengths the file contains all parameters used during the measurement. Additionally the mapping of all measured intensities to the spectral order they were taken from is stored.

#### Structure

The ARY or ARYX file is a zip-archive containing three or four files of different format, “.~tmp”, “.~aif”, “.~rep” for ARY or ".~json" for ARYX and a "metadata_summary.json".

- The “.~tmp” file contains the wavelengths and intensities of the spectrum.
- The “.~aif” (additional information) is used to map the wavelengths to their spectral orders.
- The “.~rep” (report, ARY only) contains the measurement parameters and information about the setup used.
- The ".~json" (ARYX only) contains the full measurement metadata. Its structure is device dependent and considered **instable**.
- The "metadata_summary.json" is a versioned, flat structured metadata file with well defined keys and value types. Introduced in April 2024.

##### “.~tmp” file

The “.~tmp” file is a binary file. Wavelength and Intensity are stored in pairs of 4 byte little endian floating point numbers in ARY or as 8 byte little endian double values in ARYX. The values are primarily sorted by their spectral order and secondly by their wavelength. For a proper spectral representation they need to be sorted.

| Value  | Intensity                | Wavelength               |
| ------ | ------------------------ | ------------------------ |
| Bytes  | 4 / 8 [$^1$](#footnotes) | 4 / 8 [$^1$](#footnotes) |
| Format | FLOAT32/64               | FLOAT32/64               |

Hint: Spectral overlap of neighboring orders is a typical effect in echelle spectrometers and may lead to duplication of wavelengths in the files.

##### “.~aif” file

The “.~aif” file is a little endian binary file. It contains a table that allows the mapping of all spectral pixels to their original spectral orders. The information for every spectral order has the size of 24 bytes and is structured as follows:

| Content | Left pixel | Right pixel | Order  | Left boundary | Right boundary | EMPTY | Left wavelength          | Right wavelength         |
| ------- | ---------- | ----------- | ------ | ------------- | -------------- | ----- | ------------------------ | ------------------------ |
| Bytes   | 4          | 4           | 2      | 2             | 2              | 2     | 4 / 8 [$^1$](#footnotes) | 4 / 8 [$^1$](#footnotes) |
| Format  | UINT32     | UINT32      | UINT16 | UINT16        | UINT16         | -     | FLOAT32/64               | FLOAT32/64               |

Left pixel: Indicates the index within the array loaded from the “.~tmp” file where a spectral order begins.

Right pixel: Indicates the index within the array loaded from the “.~tmp” file where a spectral order ends.

Order: Physical value of the spectral order.

Left boundary: Left camera pixel used to calculate the spectrum of the current order.

Right boundary: Right camera pixel used to calculate the spectrum of the current order.

EMPTY: Due to data optimization.

Left WL: Wavelength at `Left Pixel`.[$^1$](#footnotes) [$^2$](#footnotes)

Right WL: Wavelength at `Right Pixel`.[$^1$](#footnotes) [$^2$](#footnotes)

##### “.~rep” file

The report file is an ansi based text file of “.ini-like” format without sections. It is only used in `ary` files

##### "metadata_summary.json"

A flat metadata summary. This is the recommended metadata file to be read. It was introduced in April 2024. It is not present in older aryx files.

##### ".~json" file

A JSON file with all measurement meta data.

##### Footnotes

$^1$: Observe the difference between ARY/ARYX.

$^2$: Depending on spectrometer geometry and order orientation, both can either be the higher or lower wavelength.

#### Examples

##### Additional information

A typical part of the additional information file for ARY may look like the following:

| Left pix    | Right pix   | Order | Lb    | Rb    | Empty | Left WL     | Right WL    |
| ----------- | ----------- | ----- | ----- | ----- | ----- | ----------- | ----------- |
| 00 00 00 00 | 9b 07 00 00 | 1e 00 | 64 00 | ff 07 | 00 00 | a8 f9 59 44 | e1 6d 61 44 |
| 9c 07 00 00 | 04 0f 00 00 | 1f 00 | 62 00 | ca 07 | 00 00 | 92 ef 52 44 | 55 f7 59 44 |
| 05 0f 00 00 | 8e 16 00 00 | 20 00 | 00 00 | 89 07 | 00 00 | 4f fb 4b 44 | 5a ed 52 44 |

And can be trans scripted to:

| Left pix | Right pix | Order | Left | Right | EMPTY | Left WL | Right WL |
| -------- | --------- | ----- | ---- | ----- | ----- | ------- | -------- |
| 0        | 1947      | 30    | 100  | 2047  | 0     | 871.901 | 901.717  |
| 1948     | 3844      | 31    | 98   | 1994  | 0     | 843.743 | 871.865  |
| 3845     | 5774      | 32    | 0    | 1929  | 0     | 815.927 | 843.709  |

## Raw images

- *.raw, legacy raw image, no additional data is stored
- *.rawx, more complete legacy format, also includes spectrometer information required to calculate spectra from the image.
- *.rawb, similar to *.raw but in binary form for faster storage and loading
